#include <MergeSort.h>
#include <iostream>

namespace Algos
{

void MergeSort::sort(std::vector<int> &tab)
{
    if (tab.size() == 1 || tab.empty())
    {
        return;
    }

    auto halfSize = tab.size() / 2;

    std::vector<int> left{tab.cbegin(), tab.cbegin() + halfSize};
    std::vector<int> right{tab.cbegin() + halfSize, tab.cend()};

    sort(left);
    sort(right);

    merge(tab, left, right);
}

void MergeSort::merge(std::vector<int> &tab, const std::vector<int> &left, const std::vector<int> &right)
{
    size_t cntLeft = 0;
    size_t cntRight = 0;

    for(int & i : tab)
    {
        if (cntLeft == left.size() && cntRight == right.size())
        {
            std::cerr << "Something is wrong >_<" << std::endl;
            std::terminate();
        }

        if (cntLeft == left.size())
        {
            i = right[cntRight++];
            continue;
        }

        if (cntRight == right.size())
        {
            i = left[cntLeft++];
            continue;
        }

        if (left[cntLeft] < right[cntRight])
        {
            i = left[cntLeft++];
            continue;
        }

        if (right[cntRight] <= left[cntLeft])
        {
            i = right[cntRight++];
        }
    }

}

unsigned InversionCounter::count(std::vector<int> &tab)
{
    if (tab.size() == 1 || tab.empty())
    {
        return 0;
    }

    auto halfSize = tab.size() / 2.;

    std::vector<int> left{tab.cbegin(), tab.cbegin() + halfSize};
    std::vector<int> right{tab.cbegin() + halfSize, tab.cend()};

    auto x = count(left);
    auto y = count(right);
    auto z = mergeAndCount(tab, left, right);


    return x+y+z;
}

unsigned
InversionCounter::mergeAndCount(std::vector<int> &tab, const std::vector<int> &left, const std::vector<int> &right)
{
    size_t cntLeft = 0;
    size_t cntRight = 0;
    unsigned reps = 0;

    for(int & i : tab)
    {
        if (cntLeft == left.size() && cntRight == right.size())
        {
            std::cerr << "Something is wrong >_<" << std::endl;
            std::terminate();
        }

        if (cntLeft == left.size())
        {
            i = right[cntRight++];
            continue;
        }

        if (cntRight == right.size())
        {
            i = left[cntLeft++];
            continue;
        }

        if (left[cntLeft] < right[cntRight])
        {
            i = left[cntLeft++];
            continue;
        }

        if (right[cntRight] <= left[cntLeft])
        {
            reps += left.size() - cntLeft;
            i = right[cntRight++];
        }
    }

    return reps;
}
} // namespace Algos
