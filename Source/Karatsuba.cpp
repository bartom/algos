#include "Karatsuba.h"
#include <iostream>


namespace Algos
{

LongNumber Karatsuba::compute(const LongNumber &x, const LongNumber &y)
{
    if (x.size()  < 2 || y.size() < 2)
    {
        return multiply(x, y);
    }

    auto mid = std::ceil(std::max(x.size(), y.size()) / 2.);

    LongNumber xLow = mid >= x.size() ? x : LongNumber{x.cbegin() + (x.size() - mid), x.cend()};
    LongNumber xHigh = mid >= x.size() ? LongNumber() : LongNumber{x.cbegin(), x.cbegin() + (x.size() - mid)};
    LongNumber yLow = mid >= y.size() ? y : LongNumber{y.cbegin() + (y.size() - mid), y.cend()};
    LongNumber yHigh = mid >= y.size() ? LongNumber() : LongNumber{y.cbegin(), y.cbegin() + (y.size() - mid)};

    auto z0 = compute(xHigh, yHigh);
    auto z1 = compute(sum(xHigh, xLow), sum(yHigh, yLow));
    auto z2 = compute(xLow, yLow);

    auto part1 = multiplyTimes10(z0, mid*2);
    auto part2 = multiplyTimes10(sub(sub(z1, z0), z2), mid);

    return sum(sum(part1, part2), z2);
}

LongNumber Karatsuba::multiply(const LongNumber &x, const LongNumber &y)
{
    if (x.size() > 10 || y.size() > 10)
    {
        std::cerr << "One or both of base numbers are too big" << std::endl;
        std::terminate();
    }

    unsigned long x1 = fromLongNumber(x);
    unsigned long y1 = fromLongNumber(y);

    auto z = x1 * y1;

    return toLongNumber(z);
}

unsigned long Karatsuba::fromLongNumber(const LongNumber &x)
{
    unsigned long x1 = 0;
    size_t index = 0;
    for (auto it = x.crbegin(); it != x.crend(); ++it)
    {
        x1 += static_cast<unsigned long>(*it) * static_cast<unsigned long>(pow(10, index++));
    }
    return x1;
}

LongNumber Karatsuba::sum(const LongNumber &x, const LongNumber &y)
{
    const LongNumber &other = x.size() > y.size() ? y : x;
    LongNumber res = other == x ? y : x;

    auto pos = res.size() - 1;
    for(auto it = other.crbegin(); it != other.crend(); ++it)
    {
        auto z = *it + res[pos];
        if (z < 10)
        {
            res[pos] = z;
        }
        else
        {
            res[pos] = z % 10;
            if (pos == 0)
            {
                res.insert(res.begin(), 1);
                break;
            }
            res[pos-1] += 1;
        }
        --pos;
    }

    return res;
}

LongNumber Karatsuba::multiplyTimes10(const LongNumber &x, unsigned int pow)
{
   auto res = x;
   for (unsigned i = 0; i < pow; ++i)
   {
       res.push_back(0);
   }
   return res;
}

LongNumber Karatsuba::sub(const LongNumber &x, const LongNumber &y)
{
   if (y.size() > x.size())
   {
       std::cerr << "Negative result not allowed" << std::endl;
       std::terminate();
   }

   auto res = x;
   auto pos = res.size() - 1;
   for (auto it = y.crbegin(); it != y.crend(); ++it)
   {
      if (res[pos] >= *it)
      {
          res[pos] -= *it;
      }
      else
      {
          if (pos == 0)
          {
              std::cerr << "Negative result not allowed" << std::endl;
              std::terminate();
          }

          auto tempPos = pos-1;
          do
          {
              if (res[tempPos] > 0)
              {
                  --res[tempPos];
                  break;
              }
              else
              {
                  if (tempPos == 0)
                  {
                      std::cerr << "Negative result not allowed" << std::endl;
                      std::terminate();
                  }
                  res[tempPos] = 9;
              }
              --tempPos;
          } while (true);

          res[pos] = res[pos] + 10 - *it;
      }
      --pos;
   }

   while(res[0] == 0)
   {
       res.erase(res.begin());
   }

   return res;
}

}