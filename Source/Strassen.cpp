#include "Strassen.h"


namespace Algos
{

namespace
{
void verifySize(const Matrix &X, const Matrix &Y)
{
    if (X.empty() || Y.empty() ||
        X.size() != Y.size() ||
        X[0].size() != Y[0].size())
    {
        std::cerr << "Something about size is wrong!";
        std::terminate();
    }
}

}


Matrix Strassen::multiply(const Matrix &X, const Matrix &Y)
{
    if (X.empty() && Y.empty())
    {
        return Matrix{Row{}};
    }

    verifySize(X, Y);

    if (X.size() == 1)
    {
        return Matrix{Row{X[0][0] * Y[0][0]}};
    }

    auto isEven = X.size() % 2 == 0;
    auto halfSize = isEven ? X.size() / 2 : X.size() / 2 + 1;

    Matrix A(halfSize, Row(halfSize, 0));
    Matrix B(halfSize, Row(halfSize, 0));
    Matrix C(halfSize, Row(halfSize, 0));
    Matrix D(halfSize, Row(halfSize, 0));

    isEven ?
    splitMatrixEven(X, A, B, C, D) :
    splitMatrixOdd(X, A, B, C, D);

    Matrix E(halfSize, Row(halfSize, 0));
    Matrix F(halfSize, Row(halfSize, 0));
    Matrix G(halfSize, Row(halfSize, 0));
    Matrix H(halfSize, Row(halfSize, 0));

    isEven ?
    splitMatrixEven(Y, E, F, G, H) :
    splitMatrixOdd(Y, E, F, G, H);

    Matrix P1 = multiply(A, sub(F, H));
    Matrix P2 = multiply(sum(A, B), H);
    Matrix P3 = multiply(sum(C, D), E);
    Matrix P4 = multiply(D, sub(G, E));
    Matrix P5 = multiply(sum(A, D), sum(E, H));
    Matrix P6 = multiply(sub(B, D), sum(G, H));
    Matrix P7 = multiply(sub(A, C), sum(E, F));

    Matrix I = sum(sub(sum(P5, P4), P2), P6);
    Matrix J = sum(P1, P2);
    Matrix K = sum(P3, P4);
    Matrix L = sub(sub(sum(P1, P5), P3), P7);

    return isEven ? gatherMatrixEven(I, J, K, L) : gatherMatrixOdd(I, J, K, L);
}

void Strassen::splitMatrixEven(const Matrix &input, Matrix &A, Matrix &B, Matrix &C, Matrix &D)
{
    assert(input.size() == A.size() * 2);
    auto len = A.size();
    for(size_t i = 0; i < len; ++i)
    {
        for(size_t j = 0; j < len; ++j)
        {
            A[i][j] = input[i][j];
            B[i][j] = input[i][j+len];
            C[i][j] = input[i+len][j];
            D[i][j] = input[i+len][j+len];
        }
    }
}

void Strassen::splitMatrixOdd(const Matrix &input, Matrix &A, Matrix &B, Matrix &C, Matrix &D)
{
    assert(input.size() == A.size() * 2 - 1);
    auto len = A.size();
    for(size_t i = 0; i < len; ++i)
    {
        for(size_t j = 0; j < len; ++j)
        {
            A[i][j] = input[i][j];
            if (i != len-1)
            {
                C[i][j] = input[i+len][j];
                if (j != len-1)
                {
                    D[i][j] = input[i+len][j+len];
                }
            }
            if (j != len-1)
            {
                B[i][j] = input[i][j+len];
            }
        }
    }

}

Matrix Strassen::sum(const Matrix &X, const Matrix &Y)
{
    if (X.empty() && Y.empty())
    {
        return Matrix{Row{}};
    }
    verifySize(X, Y);

    auto len = X.size();
    Matrix res(len, Row(len));

    for (size_t i = 0; i < len; ++i)
    {
        for (size_t j = 0; j < len; ++j)
        {
            res[i][j] = X[i][j] + Y[i][j];
        }
    }

    return res;
}

Matrix Strassen::sub(const Matrix &X, const Matrix &Y)
{
    if (X.empty() && Y.empty())
    {
        return Matrix{Row{}};
    }
    verifySize(X, Y);

    auto len = X.size();
    Matrix res(len, Row(len));

    for (size_t i = 0; i < len; ++i)
    {
        for (size_t j = 0; j < len; ++j)
        {
            res[i][j] = X[i][j] - Y[i][j];
        }
    }

    return res;
}

Matrix Strassen::gatherMatrixEven(const Matrix &A, const Matrix &B, const Matrix &C, const Matrix &D)
{
    auto len = A.size();
    Matrix res(len * 2, Row(len * 2));

    for (size_t i = 0; i < len; ++i)
    {
        for (size_t j = 0; j < len; ++j)
        {
            res[i][j] = A[i][j];
            res[i][j+len] = B[i][j];
            res[i+len][j] = C[i][j];
            res[i+len][j+len] = D[i][j];
        }
    }

    return res;
}

Matrix Strassen::gatherMatrixOdd(const Matrix &A, const Matrix &B, const Matrix &C, const Matrix &D)
{
    auto len = A.size();
    Matrix res(len * 2 - 1, Row(len * 2 - 1));

    for (size_t i = 0; i < len; ++i)
    {
        for (size_t j = 0; j < len; ++j)
        {
            res[i][j] = A[i][j];
            if (j != len-1)
            {
                res[i][j+len] = B[i][j];
            }
            if (i != len-1)
            {
                res[i+len][j] = C[i][j];
                if (j != len-1)
                {
                    res[i+len][j+len] = D[i][j];
                }
            }
        }
    }

    return res;
}

}