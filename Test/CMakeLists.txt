add_executable(AlgosTests MergeSortTestSuite.cpp KaratsubaTestSuite.cpp StrassenTestSuite.cpp)

target_link_libraries(AlgosTests libalgos gtest_main gmock_main)
