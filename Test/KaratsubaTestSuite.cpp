#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Karatsuba.h"


using namespace testing;

namespace Algos {

TEST(LongNumber, TestConversion) {
    unsigned long x = 12345678;
    auto x1 = Karatsuba::toLongNumber(x);

    ASSERT_THAT(x1, ElementsAre(1,2,3,4,5,6,7,8));
}

TEST(LongNumber, TestFromConversion) {
    LongNumber x({1,2,3,4,5,6,7,8,9});
    auto x1 = Karatsuba::fromLongNumber(x);

    ASSERT_EQ(x1, 123456789);
}

TEST(LongNumber, TestSumSimple) {
    LongNumber x({2, 3});
    LongNumber y({5, 5});

    auto z = Karatsuba::sum(x, y);
    ASSERT_THAT(z, ElementsAre(7, 8));
}

TEST(LongNumber, TestSumSimpleOther) {
    LongNumber x({2, 3});
    LongNumber y({4, 5, 5});

    auto z = Karatsuba::sum(x, y);
    ASSERT_THAT(z, ElementsAre(4, 7, 8));
}

TEST(LongNumber, TestSumComplex) {
    LongNumber x({2, 3});
    LongNumber y({9, 9});

    auto z = Karatsuba::sum(x, y);
    ASSERT_THAT(z, ElementsAre(1, 2, 2));
}

TEST(LongNumber, TestSumComplexOther) {
    LongNumber x({1, 2, 3});
    LongNumber y({9, 9});

    auto z = Karatsuba::sum(x, y);
    ASSERT_THAT(z, ElementsAre(2, 2, 2));
}

TEST(LongNumber, TestSubSimple) {
    LongNumber x({5, 6});
    LongNumber y({1, 1});

    auto z = Karatsuba::sub(x, y);
    ASSERT_THAT(z, ElementsAre(4, 5));
}

TEST(Karatsuba, TestSimpleLongNumber) {
    LongNumber x({5});
    LongNumber y({9});

    ASSERT_THAT(Karatsuba::compute(x, y), ElementsAre(4, 5));
}

TEST(Karatsuba, TestComplexLongNumber) {
    LongNumber x({2, 3, 0, 9, 4, 8});
    LongNumber y({6, 0, 9, 8, 2, 1, 6, 3});

    ASSERT_THAT(Karatsuba::compute(x, y), ElementsAre(1, 4, 0, 8, 3, 7, 0, 8, 5, 8, 0, 5, 2, 4));
}

}