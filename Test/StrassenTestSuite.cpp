#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <vector>
#include "Strassen.h"

namespace Algos {

void assertMatrixEqual(const Matrix &A, const Matrix &B)
{
    for (size_t i = 0; i < A.size(); ++i)
    {
        for (size_t j = 0; j < A.size(); ++j)
        {
            ASSERT_EQ(A[i][j], B[i][j]);
        }
    }
}

TEST(Matrix, TestMatrixSub)
{
    Matrix m1{{3, 3, 3}, {4, 4, 4}, {5, 5, 5}};
    Matrix m2{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};

    Matrix result{{2, 2, 2}, {3, 3, 3}, {4, 4, 4}};

    assertMatrixEqual(Strassen::sub(m1, m2), result);
}

TEST(Matrix, TestMatrixSum)
{
    Matrix m1{{3, 3, 3}, {4, 4, 4}, {5, 5, 5}};
    Matrix m2{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};

    Matrix result{{4, 4, 4}, {5, 5, 5}, {6, 6, 6}};

    assertMatrixEqual(Strassen::sum(m1, m2), result);
}

TEST(Strassen, TestMatrixMulOdd)
{
    Matrix m1{{2, 3, 4}, {5, 6, 7}, {8, 9, 3}};
    Matrix m2{{6, 1, 4}, {0, 3, 1}, {4, 2, 2}};

    Matrix result{{28, 19, 19}, {58, 37, 40}, {60, 41, 47}};

    assertMatrixEqual(Strassen::multiply(m1, m2), result);
}

TEST(Strassen, TestMatrixMulEven)
{
    Matrix m1{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
    Matrix m2{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};

    Matrix result{{10, 10, 10, 10}, {26, 26, 26, 26}, {42, 42, 42, 42}, {58, 58, 58, 58}};

    assertMatrixEqual(Strassen::multiply(m1, m2), result);
}

}
