#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <vector>
#include "MergeSort.h"

using namespace testing;

TEST(Vector, TestVectorSplit) {
    std::vector<int> inVec({1, 2, 3, 4, 5, 6});

    auto halfSize = inVec.size() / 2;
    std::vector<int> outVec1{inVec.cbegin(), inVec.cbegin() + halfSize};
    std::vector<int> outVec2{inVec.cbegin() + halfSize, inVec.cend()};

    ASSERT_THAT(outVec1, ElementsAre(1, 2, 3));
    ASSERT_THAT(outVec2, ElementsAre(4, 5, 6));
}

TEST(Vector, TestVectorSplitUneven) {
    std::vector<int> inVec({1, 2, 3, 4, 5, 6, 7});

    auto halfSize = inVec.size() / 2;
    std::vector<int> outVec1{inVec.cbegin(), inVec.cbegin() + halfSize};
    std::vector<int> outVec2{inVec.cbegin() + halfSize, inVec.cend()};

    ASSERT_THAT(outVec1, ElementsAre(1, 2, 3));
    ASSERT_THAT(outVec2, ElementsAre(4, 5, 6, 7));
}

namespace Algos {

TEST(MergeSort, TestEmptyVector) {
    std::vector<int> nil;

    MergeSort::sort(nil);

    ASSERT_TRUE(nil.empty());
}

TEST(MergeSort, TestOneElement) {
    std::vector<int> single({6});

    MergeSort::sort(single);

    ASSERT_EQ(single.size(), 1);
    ASSERT_THAT(single, ElementsAre(6));
}

TEST(MergeSort, TestSimpleCase) {
    std::vector<int> out({6, 5, 4, 3, 2, 1});

    MergeSort::sort(out);

    ASSERT_THAT(out, ElementsAre(1, 2, 3, 4, 5, 6));
}

TEST(MergeSort, TestSimpleCase2) {
    std::vector<int> out({6, 5, 4, 9, 13, 2, 1});

    MergeSort::sort(out);

    ASSERT_THAT(out, ElementsAre(1, 2, 4, 5, 6, 9, 13));
}

TEST(MergeSort, TestSameElements) {
    std::vector<int> out({6, 2, 4, 2, 13, 2, 1});

    MergeSort::sort(out);

    ASSERT_THAT(out, ElementsAre(1, 2, 2, 2, 4, 6, 13));
}

TEST(InversionCounter, TestNoInversions) {
    std::vector<int> data({1,2,3,4,5,6});

    ASSERT_EQ(0, InversionCounter::count(data));
}

TEST(InversionCounter, TestMaxInversions) {
    std::vector<int> data({6,5,4,3,2,1});

    ASSERT_EQ(15, InversionCounter::count(data));
}

TEST(InversionCounter, TestSomeInversions) {
    std::vector<int> data({1,3,5,2,4,6});

    ASSERT_EQ(3, InversionCounter::count(data));

}

}
