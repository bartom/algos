#pragma once
#include<vector>


namespace Algos
{

// simple implementation, only works for integers
class MergeSort
{
public:
    static void sort(std::vector<int> &tab);

private:
    static void merge(std::vector<int> &tab, const std::vector<int> &left, const std::vector<int> &right);
};

// it's basically merge sort so I'm leaving it in this file
class InversionCounter
{
public:
    static unsigned count(std::vector<int> &tab);

private:
    static unsigned mergeAndCount(std::vector<int> &tab, const std::vector<int> &left, const std::vector<int> &right);

};

} // namespace Algos
