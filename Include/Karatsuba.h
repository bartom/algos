#pragma once
#include <vector>
#include <algorithm>

namespace Algos
{

using LongNumber = std::vector<uint8_t>;

class Karatsuba
{
public:
    static LongNumber compute(const LongNumber &x, const LongNumber &y);

    template<class T, typename std::enable_if<
            std::is_integral<T>::value &&
            !std::is_same<T, bool>::value &&
            !std::is_signed<T>::value
            ,T>::type* = nullptr>
    static LongNumber toLongNumber(T number)
    {
        LongNumber res;
        while (number != 0)
        {
            res.push_back(number % 10);
            number /= 10;
        }

        std::reverse(res.begin(), res.end());
        return res;
    }

    static unsigned long fromLongNumber(const LongNumber &x);
    static LongNumber sum(const LongNumber &x, const LongNumber &y);
    static LongNumber sub(const LongNumber &x, const LongNumber &y);

private:
    static LongNumber multiply(const LongNumber &x, const LongNumber &y);
    static LongNumber multiplyTimes10(const LongNumber &x, unsigned pow);
};

}