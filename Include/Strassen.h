#pragma once
#include <vector>
#include <iostream>

namespace Algos
{

using Row = std::vector<int>;
using Matrix = std::vector<Row>; // simple square matrix representation

class Strassen
{
public:
    static Matrix multiply(const Matrix &X, const Matrix &Y);

    static Matrix sum(const Matrix &X, const Matrix &Y);
    static Matrix sub(const Matrix &X, const Matrix &Y);

private:
    static void splitMatrixEven(const Matrix &input, Matrix &A, Matrix &B, Matrix &C, Matrix &D);
    static void splitMatrixOdd(const Matrix &input, Matrix &A, Matrix &B, Matrix &C, Matrix &D);

    static Matrix gatherMatrixEven(const Matrix &A, const Matrix &B, const Matrix &C, const Matrix &D);
    static Matrix gatherMatrixOdd(const Matrix &A, const Matrix &B, const Matrix &C, const  Matrix &D);
};


}